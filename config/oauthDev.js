const admin = require("firebase-admin");
const simple_oauth2 = require('simple-oauth2');
const rp = require('request-promise');

const serviceAccount = require('../functions/service-account.json');
const adminConfig = {
	apiKey: "AIzaSyBFFvT-vyYIMxoEIpldF0Liuz58i5MtRvg",
	authDomain: "musicbotdb-d2a1a.firebaseapp.com",
	databaseURL: "https://musicbotdb-d2a1a.firebaseio.com",
	projectId: "musicbotdb-d2a1a",
	storageBucket: "musicbotdb-d2a1a.appspot.com",
	messagingSenderId: "550670441433"
}

adminConfig.credential = admin.credential.cert(serviceAccount);
admin.initializeApp(adminConfig);

const db = admin.firestore();

const DISCORD_CLIENT_ID = '381144829570318336';
const DISCORD_CLIENT_SECRET = '9QcWQhwg0AIiOpv-SdygyIqh63p-lc5l';

const credentials = {
  client: {
    id: DISCORD_CLIENT_ID,
    secret: DISCORD_CLIENT_SECRET
  },
  auth: {
    tokenHost: 'https://discordapp.com',
    tokenPath: '/api/oauth2/token',
    revokePath: '/api/oauth2/token/revoke',
	authorizePath: '/api/oauth2/authorize',
  }
};

const oauth2 = simple_oauth2.create(credentials);
const OAUTH_REDIRECT_PATH = '/redirect';
const OAUTH_CALLBACK_PATH = '/callback';
const OAUTH_SCOPES = ['identify','guilds','email'].join(' ');


function getFromApi(path, accessToken) {
    return rp({
	    uri: `https://discordapp.com/api/v6${path}`,	    
	    headers: {
	        'Content-Type': 'application/x-www-form-urlencoded',
	        'Authorization': `Bearer ${accessToken}`
	    },
	    json: true
	})
}

function createFirebaseAccount(userInfo, accesses) {
  let { id, username, avatar, discriminator } = userInfo.user

  const uid = `discord:${id}`,
  		photoURL = `https://cdn.discordapp.com/avatars/${id}/${avatar}.png`;

  // Save the access tokens ans user info to the Firestore Database.
  const databaseTask = db.doc(`/users/${uid}`)
      .set({ username, avatar, discriminator }); // { userInfo, accesses }

  let guildsTasks = []
  userInfo.guilds.forEach(guild => {
    let { icon, name, owner, permissions } = guild

    guildsTasks.push(
      db.doc(`/users/${uid}/guilds/${guild.id}`).set({ icon, name, owner, permissions })
    )
  })

  // Create or update the user account.
  const userCreationTask = admin.auth().updateUser(uid, {
    displayName: username,
    photoURL
  }).catch(error => {
    // If user does not exists we create it.
    if (error.code === 'auth/user-not-found') {
      return admin.auth().createUser({
        uid,
        displayName: username,
        photoURL
      });
    }
    throw error;
  });

  // Wait for all async task to complete then generate and return a custom auth token.
  return Promise.all([userCreationTask, databaseTask, ...guildsTasks]).then(() => {
    // Create a Firebase custom auth token.
    const token = admin.auth().createCustomToken(uid);
    console.log('Created Custom token for UID "', uid, '" Token:', token);

    return token;
  });
}

function signInFirebaseTemplate(token) {
  return `
	<script src="https://www.gstatic.com/firebasejs/5.0.3/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/5.0.3/firebase-auth.js"></script>
    <script>
      var token = '${token}';
      var config = {
        apiKey: '${adminConfig.apiKey}'
      };
      var app = firebase.initializeApp(config);
      app.auth().signInWithCustomToken(token).then(function() {
        window.close();
      });
    </script>`;
}

module.exports.init = function(app) {
	app.get(OAUTH_REDIRECT_PATH, (req, res) => {
	  const redirectUri = oauth2.authorizationCode.authorizeURL({
	    redirect_uri: `${req.protocol}://${req.get('host')}${OAUTH_CALLBACK_PATH}`,
	    scope: OAUTH_SCOPES,
	    response_type: 'code'
	  });

	  console.log('Redirecting to:', redirectUri);
	  res.redirect(redirectUri);
	});

	app.get(OAUTH_CALLBACK_PATH, (req, res) => {
	  console.log('Received auth code:', req.query.code);
	  
	  if(!req.query.code || req.query.code.length <= 0) {
	  	return res.send('<script>window.close();</script>');
	  }

	  oauth2.authorizationCode.getToken({
	    code: req.query.code,
	    redirect_uri: `${req.protocol}://${req.get('host')}${OAUTH_CALLBACK_PATH}`
	  }).then(results => {
	  	console.log('Auth code exchange result received:', results);
	  	let { access_token, token_type, expires_in, refresh_token, scope } = results;

	  	Promise.all([
	  		getFromApi('/users/@me', access_token),
	  		getFromApi('/users/@me/guilds', access_token)
	  	]).then(data => {
	  		let [ user, guilds ] = data
	  		// Create a Firebase account and get the Custom Auth Token.
	  		createFirebaseAccount({ user, guilds }, results).then(firebaseToken => {
	  		  // Serve an HTML page that signs the user in and updates the user profile.
	  		  
	  		  // res.redirect(`https://musicbot.brain-dev.com/authenticate?token=${firebaseToken}`);
	  		  res.send(signInFirebaseTemplate(firebaseToken));
	  		}).catch(console.error);
	  	}).catch(console.error);
	  }).catch(console.error);
	});
}
/*


const api = functions.https.onRequest(app)

module.exports = {
  api
}*/

// exports.api = functions.https.onRequest((req, res) => {
// 	let { token } = req.query;

// 	if (token) {
// 		admin.auth().verifyIdToken(token)
// 			.then(authUser => res.json(authUser))
// 			.catch(console.error);
// 	}

// 	res.status(401).send('nope');
// });
