# Music Bot admin web interface

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Before Build

Create `src/config.js` file and setup your api keys

```javascript
const youtubeConfig = {
  ApiKey: 'your-youtube-api-key'
}

const firebaseConfig = {
  apiKey: 'your-apikey',
  authDomain: 'your-auth-domain',
  databaseURL: 'your-database-url',
  projectId: 'your-project-id',
  storageBucket: 'your-storage-bucket',
  messagingSenderId: 'your-messaing-sender-id'
}

export {
  youtubeConfig,
  firebaseConfig
}
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
