/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: null,
        currentGuild: null
    },

    mutations: {
        updateUser (state, user) {
            if (!user) {
                state.user = null
            } else {
                let { uid, displayName, photoURL } = user

                state.user = { uid, displayName, photoURL }
            }
        },

        updateCurrentGuild (state, guild = null) {
            state.currentGuild = guild
        }
    },

    actions: {
        // getUserInfo ({ commit }) {
        //     console.log(this)       
        // }
    },

    strict: debug
})
