// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import Vuetify from 'vuetify'
import router from './router'
import store from './store'
import App from './App'

import VueFire from 'vuefire'
import FirebaseDB from '@/firebase/plugin'

import '@/mixins/ReBindFirestore'

Vue.config.productionTip = false

Vue.use(FirebaseDB)
Vue.use(VueFire)
Vue.use(Vuetify)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})

/*
    Content-Type    application/x-www-form-urlencoded
    Authorization   Bearer vRZ8t9eF49M2RNi0VnE7gaDdyrOfGI

    https://discordapp.com/api/v6/users/@me
    {
        "username": "Reez.ftw",
        "verified": true,
        "mfa_enabled": false,
        "id": "286582407434600449",
        "avatar": "c298c0b79eeec8b4357c76abd4844a96",
        "discriminator": "9235",
        "email": "myrzz0r@gmail.com"
    }

    https://cdn.discordapp.com
    Avatar url
        /avatars/<user_id>/<user_avatar>.png
        example:
        https://cdn.discordapp.com/avatars/286582407434600449/c298c0b79eeec8b4357c76abd4844a96.png

    Guild Icon
        /icons/<guild_id>/<guild_icon>.png

    https://discordapp.com/api/v6/users/@me/guilds
    [
        ...,
        {
            "owner": false,
            "permissions": 2146958591,
            "icon": "bbfa000469cfc288bff3f27f46d3be75",
            "id": "286562424121720832",
            "name": "Мужчинасы"
        },
        ...
    ]
*/
