import Vue from 'vue'

Vue.mixin({
  methods: {
    _reBindFirestore () {
      this._firestoreUnbinds = Object.create(null)
      this.$firestoreRefs = Object.create(null)

      let refs = this._getFirestoreRefs()
      if (!refs) { return }

      Object.keys(refs).forEach(key => {
        this.$bind(key, refs[key])
      })
    },

    _getFirestoreRefs () {
      let { firestore, dontReBind } = this.$options
      if (dontReBind) return false

      return typeof firestore === 'function' ? firestore.call(this) : firestore
    }
  },
  created () {
    let refs = this._getFirestoreRefs()
    if (!refs) { return }
    this.$root.$on('currentGuildChanged', this._reBindFirestore)
  },
  beforeDestroy () {
    let refs = this._getFirestoreRefs()
    if (!refs) { return }
    this.$root.$off('currentGuildChanged', this._reBindFirestore)
  }
})
