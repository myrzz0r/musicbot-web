import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Search from '@/components/Search'
import Videos from '@/components/Videos'
import Playlists from '@/components/Playlists'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: { name: 'search' },
      name: 'home',
      component: Home
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/plalists',
      name: 'plalists',
      component: Playlists
    },
    {
      path: '/videos',
      name: 'videos',
      component: Videos
    },
    {
      path: '/settings',
      name: 'settings',
      component: Search
    }
  ]
})
