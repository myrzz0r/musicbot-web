/* eslint-disable */ 
import { youtubeConfig } from '@/config'
import axios from 'axios'

const { ApiKey } = youtubeConfig,
  ROOT_URL = 'https://www.googleapis.com/youtube/v3';

function baseRequest(type, options) {
  let LINK = `${ROOT_URL}/${type}`,
    params = {
      key: ApiKey,
      maxResults: 50,
      part: 'snippet',
      ...options
    }

  return new Promise((resolve, reject) => {
    axios.get(LINK, { params })
    .then(({ data }) => {
      resolve(data)
    }).catch(reject)
  })
}

function search (q, opts = {}) {
  let options = {
      maxResults: 25,
      ...opts,      
      type: 'video',
      q
    },
    list = [];
   
  return new Promise((resolve, reject) => {
    baseRequest('search', options).then(({ items, pageInfo, nextPageToken }) => {
      if(items.length) {
        list = [ ...list, ...items];
      }
      resolve({ list, pageInfo, nextPageToken });
    }).catch(reject);
  }).then(data => {
    let list = data.list.map(item => {
      return {
        id: item.id.videoId,
        title: item.snippet.title,
        thumbnails: item.snippet.thumbnails,
        url: `https://www.youtube.com/watch?v=${item.id.videoId}`
      }
    });
    return { ...data, list }
  });
}

function videos (id, opts = {}) {
  let options = {
      maxResults: 25,
      part: 'id,snippet',
      ...opts,
      id
    },
    list = [];
   
  return new Promise((resolve, reject) => {
    baseRequest('videos', options).then(({ items, pageInfo, nextPageToken }) => {
      if(items.length)
        list = items;

      resolve({ list, pageInfo, nextPageToken });      
    }).catch(reject);
  }).then(data => {
    let list = data.list.map(item => {
      return {
        id: item.id,
        title: item.snippet.title,
        thumbnails: item.snippet.thumbnails,
        url: `https://www.youtube.com/watch?v=${item.id}`
      }
    });
    return { ...data, list }
  });
}

function _getPlaylist(options) {
  let result = {
    list: [],
    pageInfo: null,
  }

  return new Promise((resolve, reject) => {
    baseRequest('playlistItems', options).then(({ items, pageInfo, nextPageToken }) => {
      result.pageInfo = pageInfo

      if(items.length) {
        result.list = [ ...result.list, ...items];
      }

      if(nextPageToken) {
        _getPlaylist({
          ...options,
          pageToken: nextPageToken
        }).then(data => {
          result.list = [ ...result.list, ...data.list];
          resolve(result);
        })
      } else {
        resolve(result);
      }         
    }).catch(reject);
  })
}

function playlist(playlistId, opts = {}) {
  let options = {
      maxResults: 50,
      ...opts,
      part: 'snippet,contentDetails',
      playlistId
    },
    fullVideoList = [];
   
  return _getPlaylist(options).then(data => {
    let list = data.list.map(({ snippet }) => {
      return {
        id: snippet.resourceId.videoId,
        title: snippet.title,
        thumbnails: snippet.thumbnails,
        url: `https://www.youtube.com/watch?v=${snippet.resourceId.videoId}`
      }
    })

    return { ...data, list }
  });
}

function _getParams(link) {
  let res = {},
    [ a, ...params ] = link.split('?');

  params.shift().split('&').forEach(item => {
    let [key, value ] = item.split("=");
    res[key] = value;
  });

  return res
}

function get(query, options = {}) {
  // https://www.youtube.com/playlist?list=
  if(query.includes('/playlist')) {
    let { list } = _getParams(query);
    return playlist(list, options);
  }

  // https://www.youtube.com/watch?v=
  if(query.includes('/watch')) {
    let { v } = _getParams(query);
    return videos(v, options);
  }
  // Просто поиск по тексту
  return search(query, options);
}

export {
  search,
  videos,
  playlist,
  get
}
