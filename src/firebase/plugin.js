import { firebaseConfig } from '@/config'
import { firebase } from '@firebase/app'
import '@firebase/firestore'
import '@firebase/auth'

const firebaseApp = firebase.initializeApp(firebaseConfig)
const db = firebaseApp.firestore()

db.settings({ timestampsInSnapshots: true })

let install = function (_vue) {
  _vue.prototype.$db = db
  _vue.prototype.$auth = firebase.auth()
}

export default install
